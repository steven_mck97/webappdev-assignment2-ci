import loglevel from 'loglevel';
import userModel from '../api/users/userModel';
import movieModel from '../api/movies/movieModel';
import tvShowModel from '../api/tvShows/tvShowModel';
import peopleModel from '../api/people/peopleModel';
import {movies} from './movies.js';
import {tvShows} from './tvShows.js';
import {people} from './people.js';

const users = [
  {
    'username': 'user1',
    'password': 'test1',
  },
  {
    'username': 'user2',
    'password': 'test2',
  },
];

// deletes all user documents in collection and inserts test data
export async function loadUsers() {
  loglevel.info('load user Data');
    try {
      await userModel.deleteMany({});
      await userModel.collection.insertMany(users);
      loglevel.info(`${users.length} users were successfully stored.`);
    } catch (err) {
      console.error(`failed to Load user Data: ${err}`);
    }
  }

  export async function loadMovies() {
    console.log('load seed data');
    console.log(movies.length);
    try {
      await movieModel.deleteMany();
      await movieModel.collection.insertMany(movies);
      console.info(`${movies.length} Movies were successfully stored.`);
    } catch (err) {
      console.error(`failed to Load movie Data: ${err}`);
    }
  }

  export async function loadTvShows() {
    console.log('load seed data');
    console.log(tvShows.length);
    try {
      await tvShowModel.deleteMany();
      await tvShowModel.collection.insertMany(tvShows);
      console.info(`${tvShows.length} TV Shows were successfully stored.`);
    } catch (err) {
      console.error(`failed to Load TV show Data: ${err}`);
    }
  }

  export async function loadPeople() {
    console.log('load seed data');
    console.log(people.length);
    try {
      await peopleModel.deleteMany();
      await peopleModel.collection.insertMany(people);
      console.info(`${people.length} People were successfully stored.`);
    } catch (err) {
      console.error(`failed to Load people Data: ${err}`);
    }
  }