import mongoose from 'mongoose';

const Schema = mongoose.Schema;


const PeopleSchema = new Schema({
  birthday: { type: String },
  known_for_department: { type: String },
  id: { type: Number, required: true, unique: true },
  name: { type: String },
  biography: { type: String },
  popularity: { type: Number },
  imdb_id: { type: String },
  profile_path: { type: String },
  place_of_birth: { type: Number },
});

PeopleSchema.statics.findByPersonDBId = function (id) {
  return this.findOne({ id: id });
};

export default mongoose.model('People', PeopleSchema);


