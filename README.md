# Assignment 2 - Agile Software Practice.

Name: Steven McKeown

## Target Web API.

...... Document the Web API that is the target for this assignment's CI/CD pipeline. Include the API's endpoints and any other features relevant to the creation of a suitable pipeline, e.g.

+ Get /api/movies - returns an array of movie objects.
+ Get /api/movies/:id - returns detailed information on a specific movie.
+ Get /api/movies/:id/reviews - returns a list of movie reviews.
+ Get /api/movies/upcoming - returns an array of upcoming movies.
+ Get /api/movies/popular - returns an array of popular movies.
+ Get /api/movies/now_playing - returns an array of now playing movies.
+ Get /api/people - returns an array of people objects.
+ Get /api/people/:id - returns detailed information on a specific person.
+ Get /api/tvShows - returns an array of tv show objects.
+ Get /api/tvShows/:id - returns detailed information on a specific tv show.
+ Get /api/tvShows/:id/reviews - returns a list of tv show reviews.
+ Get /api/tvShows/airing - returns an array of airing tv shows.
+ Get /api/tvShows/topRated - returns an array of top rated tv shows.
+ Get /users - returns an array of all users.
+ Post /users - registers or authenticates a user, has validation.
+ Put /users - Updates a user stored on mongodb.
+ Post /users/userNames/favourites - adds a user's favourite movie to the db.
+ Get /users/userNames/favourites - returns an array of a user's favourite movies.

## Error/Exception Testing.

.... From the list of endpoints above, specify those that have error/exceptional test cases in your test code, the relevant test file and the nature of the test case(s), e.g.

+ Get /api/movies - Test when user is authenticated to show that the get movies api call should return 20 movies and a status 200. See tests/functional/api/movies/index.js 
+ Get /api/movies/:id - Tests an authenticated user when the id is valid that it should return the movie that matches the specified id. Also does the opposite test for when an id is invalid and should return not found. See tests/functional/api/movies/index.js 
+ Get /api/movies/upcoming - When a user is authenticated it tests to show it should return 20 upcoming movies and display a status 200. See tests/functional/api/movies/index.js 
+ Get /api/movies/popular - When a user is authenticated it tests to show it should return 20 popular movies and display a status 200. See tests/functional/api/movies/index.js 
+ Get /api/movies/now_playing - When a user is authenticated it tests to show it should return 20 now playing movies and display a status 200. See tests/functional/api/movies/index.js 

+ Get /api/people - Test when user is authenticated to show that the get people api call should return 20 people and a status 200. See tests/functional/api/people/index.js 
+ Get /api/people/:id - Tests an authenticated user when the id is valid that it should return the person that matches the specified id. Also does the opposite test for when an id is invalid and should return not found. See tests/functional/api/people/index.js 

+ Get /api/tvShows - Test when user is authenticated to show that the get tv shows api call should return 20 tv shows and a status 200. See tests/functional/api/tvShows/index.js 
+ Get /api/tvShows/:id - Tests an authenticated user when the id is valid that it should return the tv show that matches the specified id. Also does the opposite test for when an id is invalid and should return not found. See tests/functional/api/tvShows/index.js 
+ Get /api/tvShows/airing - When a user is authenticated it tests to show it should return 20 airing tv shows and display a status 200. See tests/functional/api/tvShows/index.js 
+ Get /api/tvShows/topRated- Test when user is authenticated to show that the get tv shows api call should return 20 top rated tv shows and a status 200. See tests/functional/api/tvShows/index.js 

+ Get /users - Tests when authenticated to show it should return 2 specified users.
+ Post /users - Tests to show a user with predefined credentials gets registered to the db and does a get request afterwards to show it was added.


## Continuous Delivery/Deployment.

..... Specify the URLs for the staging and production deployments of your web API, e.g.

+ https://stevenmck-movies-api-staging.herokuapp.com/ - Staging deployment
+ https://stevenmck-movies-api-prod.herokuapp.com/ - Production

.... Show a screenshots from the overview page for the two Heroku apps e,g,

+ Staging app overview 

![][herokustaging]

+ Production app overview 

![][herokuprod]

[If an alternative platform to Heroku was used then show the relevant page from that platform's UI.]

## Feature Flags (If relevant)

... Specify the feature(s) in your web API that is/are controlled by a feature flag(s). Mention the source code files that contain the Optimizerly code that implement the flags. Show screenshots (with appropriate captions) from your Optimizely account that prove you successfully configured the flags.


[herokustaging]: ./public/herokustaging.png
[herokuprod]: ./public/herokuprod.png