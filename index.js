import dotenv from 'dotenv';
import express from 'express';
import moviesRouter from './api/movies';
import bodyParser from 'body-parser';
import loglevel from 'loglevel';
import './db';
import {loadUsers, loadMovies, loadTvShows,loadPeople} from './seedData';
import usersRouter from './api/users';
import tvRouter from './api/tvShows';
import peopleRouter from './api/people';
import session from 'express-session';
import passport from './authenticate';

if (process.env.NODE_ENV === 'test') {
  loglevel.setLevel('warn')
} else {
  loglevel.setLevel('info')
}

dotenv.config();


if (process.env.SEED_DB === 'true' && process.env.NODE_ENV === 'development') {
  loadUsers();
  loadMovies();
  loadTvShows();
  loadPeople();
}
const errHandler = (err, req, res, next) => {
  /* if the error in development then send stack trace to display whole error,
  if it's in production then just send error message  */
  if(process.env.NODE_ENV === 'production') {
    return res.status(500).send(`Something went wrong!`);
  }
  res.status(500).send(`Hey!! You caught the error 👍👍, ${err.stack} `);
};

const app = express();

const helmet = require("helmet");
const port = process.env.PORT ;

app.use(helmet());

app.use(session({
  secret: 'ilikecake',
  resave: true,
  saveUninitialized: true
}));

//configure body-parser
app.use(bodyParser.json());
app.use(express.static('public'));

app.use('/api/movies', passport.authenticate('jwt', {session: false}), moviesRouter);
app.use('/api/upcoming', passport.authenticate('jwt', {session: false}), moviesRouter);
app.use('/api/tvShows', passport.authenticate('jwt', {session: false}), tvRouter);
app.use('/api/airing', passport.authenticate('jwt', {session: false}), tvRouter);
app.use('/api/topRated', passport.authenticate('jwt', {session: false}), tvRouter);
app.use('/api/people', passport.authenticate('jwt', {session: false}), peopleRouter);
app.use('/api/users', usersRouter);

app.use(errHandler);

let server = app.listen(port, () => {
  loglevel.info(`Server running at ${port}`);
});

module.exports = server;