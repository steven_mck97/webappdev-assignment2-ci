import chai from "chai";
import request from "supertest";

const expect = chai.expect;

let api;

let token;

const sampleTvShow = {
    id: 604,
    original_name: "Teen Titans",
};

describe("Tv Show Endpoint", () => {
    beforeEach(async () => {
        try {
            api = require("../../../../index");
        } catch (err) {
            console.error(`failed to Load user Data: ${err}`);
        }
    });

    afterEach(() => {
        api.close(); // Release PORT 8080
        delete require.cache[require.resolve("../../../../index")];
        return true;
    });

    describe("GET /tvShows ", () => {
        it("should return 20 TV shows and a status 200", () => {
            request(api)
                .get("/api/tvShows")
                .set("Accept", "application/json")
                .set('Authorization', 'Bearer ' + token)
                .expect("Content-Type", /json/)
                .expect(200)
                .then((err, res) => {
                    expect(res.body).to.be.a("array");
                    expect(res.body.length).to.equal(20);
                });
        });
    });

    describe("GET /tv/:id", () => {
        describe("when the id is valid", () => {
            it("should return the matching TV show", () => {
                request(api)
                    .get(`/api/tvShows/${sampleTvShow.id}`)
                    .set('Authorization', 'Bearer ' + token)
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect(200)
                    .then((res) => {
                        expect(res.body).to.have.property("name", sampleTvShow.original_name);
                    });
            });
        });

        describe("when the id is invalid", () => {
            it("should return the NOT found message", () => {
                request(api)
                    .get("/api/tvShows/xxx")
                    .set('Authorization', 'Bearer ' + token)
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect({
                        success: false,
                        status_code: 34,
                        status_message: "The resource you requested could not be found.",
                    });
            });
        });
    });

    describe("GET /airing ", () => {
        it("should return 20 airing TV Shows and a status 200", () => {
            request(api)
                .get("/api/airing")
                .set('Authorization', 'Bearer ' + token)
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .then((err, res) => {
                    expect(res.body).to.be.a("array");
                    expect(res.body.length).to.equal(20);
                    
                });
        });
    });

    describe("GET /topRated ", () => {
        it("should return 20 top rated TV shows and a status 200", () => {
            request(api)
                .get("/api/topRated")
                .set('Authorization', 'Bearer ' + token)
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .then((err, res) => {
                    expect(res.body).to.be.a("array");
                    expect(res.body.length).to.equal(20);
                });
        });
    });
});

